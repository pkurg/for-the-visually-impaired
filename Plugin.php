<?php namespace Pkurg\VisuallyImpaired;

use Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {

        return [
            'Pkurg\VisuallyImpaired\Components\ForVisuallyImpaired' => 'forvisuallyimpaired',
        ];

    }

    public function registerSettings()
    {
    }

    public function boot()
    {

        Event::listen('cms.page.beforeDisplay', function ($controller, $action, $params) {
            // Adds css
            $controller->addCSS('/plugins/pkurg/visuallyimpaired/assets/css/bvi.min.css');
            $controller->addCSS('/plugins/pkurg/visuallyimpaired/assets/css/bvi-font.min.css');
            //Adds js
            $controller->addJS('/plugins/pkurg/visuallyimpaired/assets/js/responsivevoice.min.js');
            $controller->addJS('/plugins/pkurg/visuallyimpaired/assets/js/js.cookie.js');
            $controller->addJS('/plugins/pkurg/visuallyimpaired/assets/js/bvi-init.js');
            
            $controller->addJS(trans('pkurg.visuallyimpaired::lang.plugin.lang'));
            

        });
    }

}
