<?php namespace Pkurg\VisuallyImpaired\Components;

use Cms\Classes\ComponentBase;

class ForVisuallyImpaired extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'For Visually Impaired Component',
            'description' => 'For Visually Impaired Component',
        ];
    }

    public function defineProperties()
    {

        return [            

            'title' => [
                'title' => 'Title',
                'description' => 'Title text',
                'default' => 'For the visually impaired',
                'type' => 'string'                
            ],
            'class' => [
                'title' => 'cssClass',
                'description' => 'add css class',
                'default' => '',
                'type' => 'string'                
            ],
        ];

    }
}
