<?php return [
    'plugin' => [
        'name' => 'For the visually impaired',
        'description' => 'Adds settings panel for the visually impaired',
        'lang' => '/plugins/pkurg/visuallyimpaired/assets/js/bvi.js',
    ],
];